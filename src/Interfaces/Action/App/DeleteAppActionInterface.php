<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Interfaces\Action\App;

use Psr\Http\Message\ServerRequestInterface as Request;

interface DeleteAppActionInterface
{
    public function delete(Request $request, string $id): ?array;
}
