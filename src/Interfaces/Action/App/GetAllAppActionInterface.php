<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Interfaces\Action\App;

interface GetAllAppActionInterface
{
    public function getAll(): array;
}
