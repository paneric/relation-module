<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Interfaces\Action\App;

use Psr\Http\Message\ServerRequestInterface as Request;

interface GetAllPaginatedAppActionInterface
{
    public function getAllPaginated(Request $request, string $page = null): array;
}
