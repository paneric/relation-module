<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Interfaces\Action\App;

interface GetOneByIdAppActionInterface
{
    public function getOneById(string $id): ?array;
}
