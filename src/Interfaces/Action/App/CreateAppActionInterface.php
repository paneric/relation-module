<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Interfaces\Action\App;

use Psr\Http\Message\ServerRequestInterface as Request;

interface CreateAppActionInterface
{
    public function create(Request $request): ?array;
}
