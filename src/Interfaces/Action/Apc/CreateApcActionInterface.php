<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Interfaces\Action\Apc;

use Psr\Http\Message\ServerRequestInterface as Request;

interface CreateApcActionInterface
{
    public function create(Request $request): ?array;
}
