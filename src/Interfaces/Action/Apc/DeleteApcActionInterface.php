<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Interfaces\Action\Apc;

use Psr\Http\Message\ServerRequestInterface as Request;

interface DeleteApcActionInterface
{
    public function delete(Request $request, string $id): ?array;
}
