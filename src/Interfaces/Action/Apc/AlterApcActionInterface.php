<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Interfaces\Action\Apc;

use Psr\Http\Message\ServerRequestInterface as Request;

interface AlterApcActionInterface
{
    public function update(Request $request, string $id): ?array;
}
