<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Interfaces\Action\Api;

use Psr\Http\Message\ServerRequestInterface as Request;

interface CreateApiActionInterface
{
    public function create(Request $request): ?array;
    public function getStatus(): int;
}
