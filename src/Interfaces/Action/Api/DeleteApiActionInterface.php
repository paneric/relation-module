<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Interfaces\Action\Api;

use Psr\Http\Message\ServerRequestInterface as Request;

interface DeleteApiActionInterface
{
    public function delete(Request $request, string $id): ?array;
    public function getStatus(): int;
}
