<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Interfaces\Action\Api;

use Psr\Http\Message\ServerRequestInterface as Request;

interface GetAllPaginatedApiActionInterface
{
    public function getAllPaginated(Request $request, string $page): array;
    public function getStatus(): int;
}
