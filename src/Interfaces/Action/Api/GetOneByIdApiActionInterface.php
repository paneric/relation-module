<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Interfaces\Action\Api;

interface GetOneByIdApiActionInterface
{
    public function getOneById(string $id): ?array;
    public function getStatus(): int;
}
