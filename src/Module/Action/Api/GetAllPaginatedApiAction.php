<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Module\Action\Api;

use Paneric\CSRTriad\Service;
use Paneric\RelationModule\Interfaces\Action\Api\GetAllPaginatedApiActionInterface;
use Paneric\RelationModule\Interfaces\Repository\ModuleRepositoryInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetAllPaginatedApiAction extends Service implements GetAllPaginatedApiActionInterface
{
    protected $adapter;

    protected $findByCriteria;
    protected $orderBy;

    protected $status;

    public function __construct(ModuleRepositoryInterface $adapter, array $config)
    {
        parent::__construct();

        $this->adapter = $adapter;

        $this->findByCriteria = $config['find_by_criteria'];
        $this->orderBy = $config['order_by'];
    }

    public function getAllPaginated(Request $request, string $page): array
    {
        $pagination = $request->getAttribute('pagination');

        $queryParams = $request->getQueryParams();        

        $findByCriteria = $this->findByCriteria;
        $orderBy = $this->orderBy;

        $collection = $this->adapter->findBy(
            $findByCriteria(),
            $orderBy(),
            $pagination['limit'],
            $pagination['offset']
        );

        $this->status = 200;

        return [
            'status' => $this->status,
            'body' => $this->jsonSerializeObjects($collection, false),
            'pagination' => $pagination,
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}