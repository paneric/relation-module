<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Module\Action\Api;

use Paneric\CSRTriad\Service;
use Paneric\RelationModule\Interfaces\Action\Api\CreateApiActionInterface;
use Paneric\RelationModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\Validation\ValidationService;
use Psr\Http\Message\ServerRequestInterface as Request;

class CreateApiAction extends Service implements CreateApiActionInterface
{
    protected $adapter;
    protected $validation;

    protected $createUniqueCriteria;
    protected $daoClass;
    protected $dtoClass;

    protected $status;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ValidationService $validation,
        array $config
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->validation = $validation;

        $this->daoClass = $config['dao_class'];
        $this->dtoClass = $config['dto_class'];
        $this->createUniqueCriteria = $config['create_unique_criteria'];
    }

    public function create(Request $request): ?array
    {
        if (!in_array('application/json;charset=utf-8', $request->getHeader('Content-type'), true)) {
            $this->status = 400;

            return [
                'status' => $this->status,
                'error' => 'Invalid request type.'
            ];
        }

        $attributes = $request->getParsedBody();

        $validationReport = $this->validation->getReport($request);

        if (empty($validationReport[$this->dtoClass])) {

            $dao = new $this->daoClass();
            $dao->hydrate($attributes);

            $createUniqueCriteria = $this->createUniqueCriteria;

            if ($this->adapter->createUnique($createUniqueCriteria($attributes), $dao) !== null) {
                $this->status = 200;

                return [
                    'status' => $this->status
                ];
            }

            $dto = new $this->dtoClass();
            $dto->hydrate($attributes);

            $this->status = 200;

            return [
                'status' => $this->status,
                'error' => 'db_add_unique_error',
                'body' => $dto->convert()
            ];
        }

        $dto = new $this->dtoClass();
        $dto->hydrate($attributes);

        $this->status = 200;

        return [
            'status' => $this->status,
            'error' => $validationReport,
            'body' => $dto->convert()
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
