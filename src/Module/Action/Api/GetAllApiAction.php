<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Module\Action\Api;

use Paneric\CSRTriad\Service;
use Paneric\RelationModule\Interfaces\Action\Api\GetAllApiActionInterface;
use Paneric\RelationModule\Interfaces\Repository\ModuleRepositoryInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetAllApiAction extends Service implements GetAllApiActionInterface
{
    protected $adapter;

    protected $orderBy;

    protected $status;

    public function __construct(ModuleRepositoryInterface $adapter, array $config)
    {
        parent::__construct();

        $this->adapter = $adapter;

        $this->orderBy = $config['order_by'];
    }

    public function getAll(Request $request): array
    {
        $queryParams = $request->getQueryParams();

        $orderBy = $this->orderBy;

        $collection = $this->adapter->findBy(
            [],
            $orderBy($queryParams['local'])
        );

        $this->status = 200;

        return [
            'status' => $this->status,
            'body' => $this->jsonSerializeObjectsById($collection),
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
