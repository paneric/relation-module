<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Module\Action\App;

use Paneric\CSRTriad\Service;
use Paneric\RelationModule\Interfaces\Action\App\DeleteAppActionInterface;
use Paneric\RelationModule\Interfaces\Repository\ModuleRepositoryInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class DeleteAppAction extends Service implements DeleteAppActionInterface
{
    protected $adapter;

    private $moduleNameSc;
    private $findOneByCriteria;

    public function __construct(ModuleRepositoryInterface $adapter, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->adapter = $adapter;

        $this->moduleNameSc = $config['module_name_sc'];
        $this->findOneByCriteria = $config['find_one_by_criteria'];
    }

    public function delete(Request $request, string $id): ?array
    {
        if ($request->getMethod() === 'POST') {

            $findOneByCriteria = $this->findOneByCriteria;

            $this->adapter->delete($findOneByCriteria($id));

            return null;
        }

        $this->session->setFlash(['page_title' => 'form_' . $this->moduleNameSc . '_remove_title'], 'value');
        $this->session->setFlash([$this->moduleNameSc . '_delete_warning'], 'warning');

        return [];
    }
}
