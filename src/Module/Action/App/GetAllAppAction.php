<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Module\Action\App;

use Paneric\CSRTriad\Service;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\RelationModule\Interfaces\Action\App\GetAllAppActionInterface;
use Paneric\RelationModule\Interfaces\Repository\ModuleRepositoryInterface;

class GetAllAppAction extends Service implements GetAllAppActionInterface
{
    protected $adapter;

    protected $moduleNameSc;
    protected $orderBy;

    protected $prefix;

    public function __construct(ModuleRepositoryInterface $adapter, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->adapter = $adapter;

        $this->orderBy = $config['order_by'];
        $this->moduleNameSc = $config['module_name_sc'];

        $this->prefix = $config['prefix'];
    }

    public function getAll(): array
    {
        $this->session->setFlash(['page_title' => 'content_' . $this->moduleNameSc . '_show_title'], 'value');

        $orderBy = $this->orderBy;

        $local = strtolower($this->session->getData('local'));

        $collection = $this->adapter->findBy(
            [],
            $orderBy($local)
        );

        return [
            $this->prefix . 's' => $this->jsonSerializeObjectsById($collection)
        ];
    }
}
