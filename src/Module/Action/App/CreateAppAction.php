<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Module\Action\App;

use Paneric\CSRTriad\Service;
use Paneric\RelationModule\Interfaces\Action\App\CreateAppActionInterface;
use Paneric\RelationModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\Validation\ValidationService;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class CreateAppAction extends Service implements CreateAppActionInterface
{
    protected $adapter;
    protected $validation;

    protected $moduleNameSc;
    protected $createUniqueCriteria;
    protected $daoClass;
    protected $dtoClass;

    protected $prefix;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        SessionInterface $session,
        ValidationService $validation,
        array $config
    ) {
        parent::__construct($session);

        $this->adapter = $adapter;
        $this->validation = $validation;

        $this->daoClass = $config['dao_class'];
        $this->dtoClass = $config['dto_class'];
        $this->createUniqueCriteria = $config['create_unique_criteria'];
        $this->moduleNameSc = $config['module_name_sc'];

        $this->prefix = $config['prefix'];
    }

    public function create(Request $request): ?array
    {
        $attributes = $request->getParsedBody();

        if ($request->getMethod() === 'POST') {
            $validationReport = $this->validation->getReport($request);

            if (empty($validationReport[$this->dtoClass])) {
                $dao = new $this->daoClass();
                $dao->hydrate($attributes);

                $createUniqueCriteria = $this->createUniqueCriteria;

                if ($this->adapter->createUnique($createUniqueCriteria($attributes), $dao) !== null) {
                    return null;
                }

                $this->session->setFlash(['db_add_' . $this->moduleNameSc . '_unique_error'], 'error');
            }
        }

        $this->session->setFlash(['page_title' => 'form_' . $this->moduleNameSc . '_add_title'], 'value');

        $dto = new $this->daoClass();
        $dto->hydrate($attributes ?? []);

        return [
            $this->prefix => $dto->convert(),
        ];
    }
}
