<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Module\Action\App;

use Paneric\CSRTriad\Service;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\RelationModule\Interfaces\Action\App\GetOneByIdAppActionInterface;
use Paneric\RelationModule\Interfaces\Repository\ModuleRepositoryInterface;

class GetOneByIdAppAction extends Service implements GetOneByIdAppActionInterface
{
    protected $adapter;

    protected $findOneByCriteria;

    protected $prefix;

    public function __construct(ModuleRepositoryInterface $adapter, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->adapter = $adapter;

        $this->findOneByCriteria = $config['find_one_by_criteria'];

        $this->prefix = $config['prefix'];
    }

    public function getOneById(string $id): ?array
    {
        $findOneByCriteria = $this->findOneByCriteria;

        $dto = $this->adapter->findOneBy($findOneByCriteria($id));

        if ($dto ===  null) {
            return null;
        }

        return [
            $this->prefix => $dto->convert()
        ];
    }
}
