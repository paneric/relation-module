<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Module\Action\App;

use Paneric\CSRTriad\Service;
use Paneric\RelationModule\Interfaces\Action\App\AlterAppActionInterface;
use Paneric\RelationModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\Validation\ValidationService;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class AlterAppAction extends Service implements AlterAppActionInterface
{
    protected $adapter;
    protected $validation;

    protected $moduleNameSc;
    protected $daoClass;
    protected $dtoClass;
    protected $updateUniqueCriteria;
    protected $findOneByCriteria;

    private $prefix;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        SessionInterface $session,
        ValidationService $validation,
        array $config
    ) {
        parent::__construct($session);

        $this->adapter = $adapter;
        $this->validation = $validation;

        $this->moduleNameSc = $config['module_name_sc'];
        $this->daoClass = $config['dao_class'];
        $this->dtoClass = $config['dto_class'];
        $this->updateUniqueCriteria = $config['update_unique_criteria'];
        $this->findOneByCriteria = $config['find_one_by_criteria'];

        $this->prefix = $config['prefix'];
    }

    public function update(Request $request, string $id): ?array
    {
        $findOneByCriteria = $this->findOneByCriteria;

        if ($request->getMethod() === 'POST') {
            $validationReport = $this->validation->getReport($request);

            if (empty($validationReport[$this->dtoClass])) {
                $attributes = $request->getParsedBody();

                $dao = new $this->daoClass();
                $dao->hydrate($attributes);

                $updateUniqueCriteria = $this->updateUniqueCriteria;

                if ($this->adapter->updateUnique(
                        $updateUniqueCriteria($attributes, $id),
                        $findOneByCriteria($id),
                        $dao
                    ) !== null) {
                    return null;
                }

                $this->session->setFlash(['db_update_' . $this->moduleNameSc . '_unique_error'], 'error');

            }
        }

        $this->session->setFlash(['page_title' => 'form_' . $this->moduleNameSc . '_edit_title'], 'value');

        $dao = $this->adapter->findOneBy($findOneByCriteria($id));

        $dto = new $this->dtoClass();
        $dto->hydrate($dao->convert());

        return [
            $this->prefix => $dto->convert(),
        ];
    }
}
