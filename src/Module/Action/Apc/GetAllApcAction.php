<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Module\Action\Apc;

use Paneric\CSRTriad\Service;
use Paneric\HttpClient\HttpClientManager;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\RelationModule\Interfaces\Action\Apc\GetAllApcActionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetAllApcAction extends Service implements GetAllApcActionInterface
{
    protected $manager;
    protected $config;

    public function __construct(HttpClientManager $manager, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->manager = $manager;

        $this->config = $config;
    }

    public function getAll(Request $request): array
    {
        $this->session->setFlash(
            ['page_title' => 'content_' . $this->config['module_name_sc'] . '_show_title'],
            'value'
        );

        $options = [
            'headers' => [
                'Content-Type' => 'application/json;charset=utf-8',
                'Authorization' => ' Bearer ' . $request->getAttribute('token'),
            ],
            'query' => [
                'local' => strtolower($this->session->getData('local')),
            ],
        ];

        $jsonResponse = $this->manager->getJsonResponse(
            'GET',
            sprintf(
                '%s%s',
                $this->config['base_url'],
                $this->config['get_all_uri']
            ),
            $options
        );

        if ($jsonResponse['status'] === 200) {
            return [
                $this->config['prefix'] . 's' => $jsonResponse['body']
            ];
        }

        return [];
    }
}
