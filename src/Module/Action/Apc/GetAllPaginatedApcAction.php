<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Module\Action\Apc;

use Paneric\CSRTriad\Service;
use Paneric\HttpClient\HttpClientManager;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\RelationModule\Interfaces\Action\Apc\GetAllPaginatedApcActionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetAllPaginatedApcAction extends Service implements GetAllPaginatedApcActionInterface
{
    protected $manager;
    private $config;

    public function __construct(HttpClientManager $manager, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->manager = $manager;

        $this->config = $config;
    }

    public function getAllPaginated(Request $request, string $page = null): array
    {
        $this->session->setFlash(
            ['page_title' => 'content_' . $this->config['module_name_sc'] . '_show_title'],
            'value'
        );

        $options = [
            'headers' => [
                'Content-Type' => 'application/json;charset=utf-8',
                'Authorization' => ' Bearer ' . $request->getAttribute('token'),
            ],
            'query' => [
                'local' => strtolower($this->session->getData('local')),
            ],
        ];

        $jsonResponse = $this->manager->getJsonResponse(
            'GET',
            sprintf(
                '%s%s%s',
                $this->config['base_url'],
                $this->config['get_all_paginated_uri'],
                $page ?? '1'
            ),
            $options
        );

        $jsonResponse['pagination']['path'] = $this->config['origin_get_all_paginated_uri'];

        if ($jsonResponse['status'] === 200) {
            $this->session->setData($jsonResponse['pagination'], 'pagination');

            return [
                $this->config['prefix'] . 's' => $jsonResponse['body']
            ];
        }

        return [];
    }
}
