<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Module\Action\Apc;

use Paneric\CSRTriad\Service;
use Paneric\HttpClient\HttpClientManager;
use Paneric\RelationModule\Interfaces\Action\Apc\DeleteApcActionInterface;
use Paneric\Slim\Exception\HttpBadRequestException;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class DeleteApcAction extends Service implements DeleteApcActionInterface
{
    protected $manager;
    private $config;

    public function __construct(HttpClientManager $manager, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->manager = $manager;

        $this->config = $config;
    }

    public function delete(Request $request, string $id): ?array
    {
        if ($request->getMethod() === 'POST') {
            $options = [
                'headers' => [
                    'Content-Type' => 'application/json;charset=utf-8',
                    'Authorization' => ' Bearer ' . $request->getAttribute('token'),
                ],
            ];

            $jsonResponse = $this->manager->getJsonResponse(
                'DELETE',
                sprintf(
                    '%s%s%s',
                    $this->config['base_url'],
                    $this->config['delete_uri'],
                    $id
                ),
                $options
            );

            if (!isset($jsonResponse['error'])) {
                return null;
            }

            try {
                if ($jsonResponse['status'] === 400) {
                    throw new HttpBadRequestException(
                        $jsonResponse['error']
                    );
                }
            } catch (HttpBadRequestException $e) {
                error_log(sprintf(
                    "%s%s%s%s",
                    $e->getFile() . "\n",
                    $e->getLine() . "\n",
                    $e->getMessage() . "\n",
                    $e->getTraceAsString() . "\n"
                ), 0);
            }
        }

        $this->session->setFlash(
            ['page_title' => 'form_' . $this->config['module_name_sc'] . '_remove_title'],
            'value'
        );
        $this->session->setFlash(
            [$this->config['module_name_sc'] . '_delete_warning'],
            'warning'
        );

        return [];
    }
}
