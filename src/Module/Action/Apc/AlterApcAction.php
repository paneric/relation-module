<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Module\Action\Apc;

use Paneric\CSRTriad\Service;
use Paneric\HttpClient\HttpClientManager;
use Paneric\RelationModule\Interfaces\Action\Apc\AlterApcActionInterface;

use Paneric\Slim\Exception\HttpBadRequestException;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class AlterApcAction extends Service implements AlterApcActionInterface
{
    protected $manager;
    private $config;

    public function __construct(
        HttpClientManager $manager,
        SessionInterface $session,
        array $config
    ) {
        parent::__construct($session);

        $this->manager = $manager;

        $this->config = $config;
    }

    public function update(Request $request, string $id): ?array
    {
        if ($request->getMethod() === 'POST') {
            $attributes = $request->getParsedBody();

            $options = [
                'headers' => [
                    'Content-Type' => 'application/json;charset=utf-8',
                    'Authorization' => ' Bearer ' . $request->getAttribute('token'),                    
                    ],
                'query' => [
                    'local' => strtolower($this->session->getData('local')),
                ],
                'json' => $attributes,
            ];

            $jsonResponse = $this->manager->getJsonResponse(
                'PUT',
                sprintf(
                    '%s%s%s',
                    $this->config['base_url'],
                    $this->config['alter_uri'],
                    $id
                ),
                $options
            );

            if (!isset($jsonResponse['error'])) {
                return null;
            }

            if ($jsonResponse['status'] === 200) {
                $this->session->setFlash(
                    ['page_title' => 'form_' . $this->config['module_name_sc'] . '_edit_title'],
                    'value'
                );

                if (is_array($jsonResponse['error'])) {
                    return [
                        $this->config['prefix'] => $jsonResponse['body'],
                        'report' => $jsonResponse['error']
                    ];
                }

                $this->session->setFlash(
                    ['db_update_' . $this->config['module_name_sc'] . '_unique_error'],
                    'error'
                );

                return [
                    $this->config['prefix'] => $jsonResponse['body']
                ];
            }

            try {
                if ($jsonResponse['status'] === 400) {
                    throw new HttpBadRequestException(
                        $jsonResponse['error']
                    );
                }
            } catch (HttpBadRequestException $e) {
                error_log(sprintf(
                    "%s%s%s%s",
                    $e->getFile() . "\n",
                    $e->getLine() . "\n",
                    $e->getMessage() . "\n",
                    $e->getTraceAsString() . "\n"
                ), 0);
            }
        }

        $this->session->setFlash(
            ['page_title' => 'form_' . $this->config['module_name_sc'] . '_edit_title'],
            'value'
        );

        return [];
    }
}
