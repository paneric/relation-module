<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Module\Controller;

use Paneric\CSRTriad\Controller\ApiController;
use Paneric\RelationModule\Interfaces\Action\Api\CreateApiActionInterface;
use Paneric\RelationModule\Interfaces\Action\Api\AlterApiActionInterface;
use Paneric\RelationModule\Interfaces\Action\Api\DeleteApiActionInterface;
use Paneric\RelationModule\Interfaces\Action\Api\GetAllApiActionInterface;
use Paneric\RelationModule\Interfaces\Action\Api\GetAllPaginatedApiActionInterface;
use Paneric\RelationModule\Interfaces\Action\Api\GetOneByIdApiActionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class ModuleApiController extends ApiController
{
    protected $dbPrefix;

    public function __construct(string $dbPrefix)
    {
        $this->dbPrefix = $dbPrefix;
    }

    public function getAll(
        Request $request,
        Response $response,
        GetAllApiActionInterface $action
    ): Response {
        return $this->jsonResponse(
            $response,
            $action->getAll($request),
            $action->getStatus()
        );
    }

    public function getAllPaginated(
        Request $request,
        Response $response,
        GetAllPaginatedApiActionInterface $action,
        string $page = null
    ): Response {
        return $this->jsonResponse(
            $response,
            $action->getAllPaginated($request, $page),
            $action->getStatus()
        );
    }

    public function getOneById(
        Response $response,
        GetOneByIdApiActionInterface $action,
        string $id
    ): Response {
        return $this->jsonResponse(
            $response,
            $action->getOneById($id),
            $action->getStatus()
        );
    }

    public function create(
        Request $request,
        Response $response,
        CreateApiActionInterface $action
    ): Response {
        return $this->jsonResponse(
            $response,
            $action->create($request),
            $action->getStatus(),
        );
    }

    public function alter(
        Request $request,
        Response $response,
        AlterApiActionInterface
        $action,
        string $id
    ): Response {
        return $this->jsonResponse(
            $response,
            $action->update($request, $id),
            $action->getStatus(),
        );
    }

    public function delete(
        Request $request,
        Response $response,
        DeleteApiActionInterface $action,
        string $id
    ): Response {
        return $this->jsonResponse(
            $response,
            $action->delete($request, $id),
            $action->getStatus()
        );
    }
}
