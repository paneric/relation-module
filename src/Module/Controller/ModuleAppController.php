<?php

declare(strict_types=1);

namespace Paneric\RelationModule\Module\Controller;

use Paneric\CSRTriad\Controller\AppController;
use Paneric\RelationModule\Interfaces\Action\App\CreateAppActionInterface;
use Paneric\RelationModule\Interfaces\Action\App\AlterAppActionInterface;
use Paneric\RelationModule\Interfaces\Action\App\DeleteAppActionInterface;
use Paneric\RelationModule\Interfaces\Action\App\GetAllAppActionInterface;
use Paneric\RelationModule\Interfaces\Action\App\GetAllPaginatedAppActionInterface;
use Paneric\RelationModule\Interfaces\Action\App\GetOneByIdAppActionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class ModuleAppController extends AppController
{
    protected $routePrefix;

    public function __construct(Twig $twig, string $routePrefix)
    {
        parent::__construct($twig);

        $this->routePrefix = $routePrefix;
    }

    public function showAll(
        Response $response,
        array $subData,
        GetAllAppActionInterface $action
    ): Response{
        return $this->render(
            $response,
            '@module/show_all.html.twig',
            array_merge(
                $action->getAll(),
                $subData
            )
        );
    }

    public function showAllPaginated(
        Request $request,
        Response $response,
        array $subData,
        GetAllPaginatedAppActionInterface $action,
        string $page = null
    ): Response {
        return $this->render(
            $response,
            '@module/show_all_paginated.html.twig',
            array_merge(
                $action->getAllPaginated($request, $page),
                $subData
            )
        );
    }

    public function showOneById(
        Response $response,
        array $subData,
        GetOneByIdAppActionInterface $action,
        string $id
    ): Response {
        return $this->render(
            $response,
            '@module/show_one_by_id.html.twig',
            array_merge(
                $action->getOneById($id),
                $subData
            )
        );
    }

    public function add(
        Request $request,
        Response $response,
        array $subData,
        CreateAppActionInterface $action
    ): Response {
        $result = $action->create($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/' .  $this->routePrefix . '/show-all-paginated',
                200
            );
        }

        return $this->render(
            $response,
            '@module/add.html.twig',
            array_merge(
                $result,
                $subData
            )
        );
    }

    public function edit(
        Request $request,
        Response $response,
        array $subData,
        AlterAppActionInterface $action,
        string $id
    ): Response {
        $result = $action->update($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/' .  $this->routePrefix . '/show-all-paginated',
                200
            );
        }

        return $this->render(
            $response,
            '@module/edit.html.twig',
            array_merge(
                $result,
                $subData
            )
        );
    }

    public function remove(
        Request $request,
        Response $response,
        DeleteAppActionInterface $action,
        string $id
    ): Response {
        $result = $action->delete($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/' .  $this->routePrefix . '/show-all-paginated',
                200
            );
        }

        return $this->render(
            $response,
            '@module/remove.html.twig',
            ['id' => $id]
        );
    }
}
